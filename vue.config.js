module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/portfolio-admin-panel-local/' : '/',
};
