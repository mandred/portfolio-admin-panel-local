// eslint-disable-next-line import/no-extraneous-dependencies
import firebase from 'firebase/app';

export default {
  state: {
    userId: null,
  },
  getters: {
    getUserId: (state) => state.getUserId,
  },
  mutations: {
    _setUserId(state, id) {
      state.userId = id;
    },
  },
  actions: {
    async login(context, { email, password }) {
      try {
        await firebase.auth().signInWithEmailAndPassword(email, password);
      } catch (e) {
        console.warn(e);
      }
    },
    async logout({ commit }) {
      await firebase.auth().signOut();
      commit('_setUserId', null);
    },
    loadUserId() {
      const user = firebase.auth().currentUser;
      return user ? user.uid : null;
    },
  },
};
