import Vue from 'vue';
import VueRouter from 'vue-router';

// eslint-disable-next-line import/no-extraneous-dependencies
import firebase from 'firebase/app';

import AuthLogin from '../views/AuthLogin';
import ProjectsList from '../views/ProjectsList';
import ProjectEdit from '../views/ProjectEdit';
import AddProject from '../views/AddProject';
import SkillsList from '../views/SkillsList';
import SkillEdit from '../views/SkillEdit';
import AddSkill from '../views/AddSkill';
import AdminInfo from '../views/AdminInfo';
import AdminInfoEdit from '../views/AdminInfoEdit';
import AddAdminInfo from '../views/AddAdminInfo';

Vue.use(VueRouter);

const routes = [
  {
    path: '/projects',
    name: 'ProjectsList',
    meta: { auth: true },
    component: ProjectsList,
  },
  {
    path: '/project/:id',
    name: 'ProjectEdit',
    meta: { auth: true },
    component: ProjectEdit,
  },
  {
    path: '/add-project',
    name: 'AddProject',
    meta: { auth: true },
    component: AddProject,
  },
  {
    path: '/skills',
    name: 'SkillsList',
    meta: { auth: true },
    component: SkillsList,
  },
  {
    path: '/skills/:id',
    name: 'SkillEdit',
    meta: { auth: true },
    component: SkillEdit,
  },
  {
    path: '/add-skill',
    name: 'AddSkill',
    meta: { auth: true },
    component: AddSkill,
  },
  {
    path: '/admin-info',
    name: 'AdminInfo',
    meta: { auth: true },
    component: AdminInfo,
  },
  {
    path: '/add-admin-info',
    name: 'AddAdminInfo',
    meta: { auth: true },
    component: AddAdminInfo,
  },
  {
    path: '/admin-info/:id',
    name: 'AdminInfoEdit',
    meta: { auth: true },
    component: AdminInfoEdit,
  },
  {
    path: '/login',
    name: 'AuthLogin',
    component: AuthLogin,
  },
  {
    path: '*',
    redirect: ProjectsList,
    meta: { auth: true },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  firebase.auth().onAuthStateChanged((currentUser) => {
    const requireAuth = to.matched.some((item) => item.meta.auth);

    if (requireAuth && !currentUser) {
      next({ name: 'AuthLogin' });
    } else if (to.name === 'AuthLogin' && currentUser) {
      next({ name: 'ProjectsList' });
    } else {
      next();
    }
  });
});

export default router;
