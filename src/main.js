import Vue from 'vue';

import bootstrap from 'bootstrap';

import App from './App';
import './registerServiceWorker';
import router from './router';
import store from './store';
import '../firebase';

import 'bootstrap/scss/bootstrap.scss';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  bootstrap,
  render: (h) => h(App),
}).$mount('#app');
